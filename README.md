# Development

Clone all submodules in this repository by running:
```sh
git submodule update --init --recursive
```

Create your docker virtual machine. Change cpu count to the number of cores that your processor has. Change virtualbox disk size to a size that works for you. The default disk size is 20 gb but that filled up pretty fast so I changed it to 60 gb.
```sh
docker-machine create --virtualbox-cpu-count 4 --virtualbox-disk-size "60000" --virtualbox-memory "6144" --driver "virtualbox" --swarm-master default
```

When you have created your virtual docker machine, run the following command to set the environment variables so you can run docker commands from your local computer:
```sh
eval $(docker-machine env default)
```

Setup your docker swarm environment by running:
```sh
docker swarm init
```

Setup your docker swarm network by running:
```sh
docker network create -d overlay traefik-network --attachable
```
